//berisi player dari game
class Player{
    constructor(name, choosen) {
        this.name=name
        this.choosen=choosen
    }

    setChoosen(choosen) {
        this.choosen = choosen
    }

}

//berisi tampilan card 
const batu = document.getElementById('batu-p1');
batu.addEventListener('click', function onClick(event) {
  event.target.style.backgroundColor = 'rgba(196, 196, 196, 1)';
})

  const kertas = document.getElementById('kertas-p1');
  kertas.addEventListener('click', function onClick(event) {
    event.target.style.backgroundColor = 'rgba(196, 196, 196, 1)';
})

const gunting = document.getElementById('gunting-p1');
  gunting.addEventListener('click', function onClick(event) {
    event.target.style.backgroundColor = 'rgba(196, 196, 196, 1)';
})

//ini untuk game flow
class Game{
    constructor(player1, player2) {
        this.player1 = player1
        this.player2 = player2
    }

    getRandomvalue() {
        const num = Math.floor(Math.random() *3);
        let choosen,element= ''
        if(num === 0) {
            choosen='gunting'
            element=document.getElementById('gunting-p2').style.backgroundColor = 'rgba(196, 196, 196, 1)';
          
        }else if(num === 1 ) {
            choosen ='batu'
            element=document.getElementById('batu-p2').style.backgroundColor = 'rgba(196, 196, 196, 1)';
        }else if (num === 2) {
            choosen = 'kertas'
            element=document.getElementById('kertas-p2').style.backgroundColor = 'rgba(196, 196, 196, 1)';
        }
        this.player2.setChoosen(choosen)
    }

    setResult(){
        const resultText = document.getElementById('result')
        console.log(this.player1.choosen, "Pilihan Player 1")
        console.log(this.player2.choosen, "Pilihan COM")
        let result = ''
        if (this.player1.choosen === this.player2.choosen){
           result ="Draw"
        } else if (this.player1.choosen ==='gunting' && this.player2.choosen ==='kertas'){
            result = "Player WIN"
        } else if (this.player1.choosen ==='batu' && this.player2.choosen ==='gunting'){
            result = "Player WIN"
        } else if (this.player1.choosen ==='kertas' && this.player2.choosen ==='batu'){
            result = "Player WIN"
        } else{
            result = "COM WIN"
        }
        resultText.innerHTML = result
    }
}

const guntingP1=document.getElementById('gunting-p1')
const batuP1=document.getElementById('batu-p1')
const kertasP1=document.getElementById('kertas-p1')
const refreshButton = document.getElementById('refresh')
const player = new Player('player', null)
const computer = new Player('computer', null)
const game = new Game(player, computer)

guntingP1.addEventListener('click', () => {
    player.setChoosen('gunting')
    game.getRandomvalue()
    game.setResult()    
})

batuP1.addEventListener('click', () => {
    player.setChoosen('batu')
    game.getRandomvalue()
    game.setResult()
})

kertasP1.addEventListener('click', () => {
    player.setChoosen('kertas')
    game.getRandomvalue()
    game.setResult()
})

refreshButton.addEventListener('click', () => {
    window.location.reload()
})